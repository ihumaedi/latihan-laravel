@extends('layout.master')

@section('judul')
    Sign Up Form
@endsection

@section('content')
    <h1>Create New Account</h1>
    <form action="/welcome" method="post">
        @csrf
        <h3>Sign Up Form</h3>
        <label>First Name:</label><br><br>
        <input type="text" name="nama1"><br><br>
        <label>Last Name:</label><br><br>
        <input type="text" name="nama2"><br><br>

        <label>Gender:</label><br><br>
        <input type="radio" name="status">"Male"<br>
        <input type="radio" name="status">"Female"<br>
        <input type="radio" name="status">"Other"<br><br>

        <label>Nationality:</label><br><br>
        <select>
            <option>Indonesia</option>
            <option>English</option>
            <option>Arabic</option>
        </select><br><br>


        <label>Langguage Spoken:</label><br><br>
        <input type="checkbox" name="bahasa">"English"<br>
        <input type="checkbox" name="bahasa">"Indonesian"<br>
        <input type="checkbox" name="bahasa">"Other"<br><br>


        <label>Bio:</label><br><br>
        <textarea type="text" cols="50" rows="7"></textarea><br><br>
        <input type="submit" value="send">
    </form>

@endsection