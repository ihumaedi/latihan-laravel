@extends('layout.master')

@section('judul')
    Halaman index
@endsection

@section('content')
<h1>Media Online</h1>
<h3>Sosial Media Develover</h3>
<label>Belajar dan berbagi agar hidup menjadi lebih baik</label>
<h3>Benefit Join di Media Online</h3>
<ul>
    <li>Mendapatkan motivasi dari sesama develover</li>
    <li>Sharing knowledge</li>
    <li>Dibuat oleh calon web developer terbaik </li>
</ul>


<h3>Cara Bergabung ke Media Online</h3>
<ol>
    <li>Mengunjungi Website ini</li>
    <li>Mendaftarkan diri di <a href="/register">Form Sign Up</a></li>
    <li>Selesai</li>
</ol>

@endsection
